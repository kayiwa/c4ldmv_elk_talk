Run the following steps from the `demofix` directory

```
cd demofix
```
Launch the VMs

```
vagrant up
```
This will launch all your VMs.

```
vagrant ssh control
```
This logs you into your ansible control virtual machine.

```
ssh-keygen
```

Generate certificate private and public key. Do not enter a password and accept all the defaults by typing "y"

```
ssh-copy-id -i ~/.ssh/id_rsa.pub logstash
```

The vagrant user password is vagrant

Repeat the step above for the kibana and elasticsearch1 virtual machines.

```
vagrant@control:~$ ssh-copy-id -i ~/.ssh/id_rsa.pub kibana
vagrant@control:~$ ssh-copy-id -i ~/.ssh/id_rsa.pub elasticsearch1
```

Run the elasticsearch playbook to install elasticsearch on the VM.

```
ansible-playbook elasticsearch/elasticsearch-install.yml -b
```

Run the logstash playbook to install logstash on the VM

```
ansible-playbook logstash/logstash-install.yml
```

Finally install kibana from the kibana playbook

```
ansible-playbook kibana/kibana-install.yml -b
```

From the control machine. (make sure the shell says `vagrant@control:~$`) log into the logstash VM.

```
ssh logstash
```

Then let's create some logs to ship to elasticsearch by running the command below a bunch of times.

```
echo "C4L MDC Demo" >> /tmp/logstash.txt
```

Check the URL of the kibana VM from your host.

http://10.0.15.13:5601/


