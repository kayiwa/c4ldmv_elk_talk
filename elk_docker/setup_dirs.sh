#!/bin/bash

set -e

# Create data dir
if [ ! -d /home/username/collecteddata ] ; then
    echo "Creating /data directory"
    mkdir -p /home/username/collecteddata
else
    echo "Directory already exists!"
fi
